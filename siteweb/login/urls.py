from django.urls import include, re_path
from django.contrib.auth import views as auth_views
from . import forms, views

urlpatterns = [
    re_path(r'in/', auth_views.LoginView.as_view(template_name='login/login.html', authentication_form=forms.LoginForm), name='login'),
    re_path(r'out/', auth_views.LogoutView.as_view(next_page='login'), name='logout'),
 ]
