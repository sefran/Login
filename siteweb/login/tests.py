from django.test import TestCase, Client
from django.contrib.auth import authenticate
from django.contrib.auth.models import User

# Create your tests here.
class test_urls_login(TestCase):
  """Test access login screen"""
  def setUp(self):
    """Create test client"""
    self.client = Client()
  def test_login_url1_success(self):
    """Test if access http://login/"""
    response = self.client.get('/login/')
    self.assertEqual(response.status_code, 200)
  def test_login_url2_success(self):
    """Test if access http://login"""
    response = self.client.get('/login', follow=True)
    self.assertEqual(response.redirect_chain, [('/login/?next=/login', 302)])
  def test_anonymous_access(self):
    """Test anonymous access"""
    response = self.client.get('/', follow=True)
    self.assertRedirects(response, '/login/?next=/')
    response = self.client.post('/', follow=True)
    self.assertRedirects(response, '/login/?next=/')
    response = self.client.get('/admin/', follow=True)
    self.assertRedirects(response, '/login/?next=/admin/')
    response = self.client.post('/admin/', follow=True)
    self.assertRedirects(response, '/login/?next=/admin/')
    response = self.client.get('/admin', follow=True)
    self.assertRedirects(response, '/login/?next=/admin')
    response = self.client.post('/admin', follow=True)
    self.assertRedirects(response, '/login/?next=/admin')
  
class test_login(TestCase):
  """Test login user testuser"""
  def setUp(self):
    """Create test client"""
    self.client = Client()
    """Create object test user"""
    self.user = User.objects.create(username='testuser')
    self.user.set_password('motdepasse')
    self.user.save()
  def test_login_post_success(self):
    """Test if access login screen test user"""
    response = self.client.post('/login', {'username': self.user.username, 'password': 'motdepasse'}, follow=True)
    self.assertEqual(response.status_code, 200)
  def test_authenticate_success(self):
    """Test if test user login"""
    result = authenticate(username=self.user.username, password='motdepasse')
    self.assertTrue(result is not None)

class test_admin_login(TestCase):
  """Test """
  def setUp(self):
    """Create test client"""
    self.client = Client()
    """Create object test user"""
    self.user = User.objects.create(username='testuser')
    self.user.set_password('motdepasse')
    self.user.save()
  def test_login_post_success(self):
    """Test if access login screen test user to admin view"""
    response = self.client.post('/admin/login', {'username': self.user.username, 'password': 'motdpasse'}, follow=True)
    self.assertEqual(response.status_code, 200)
  def test_authenticate_success(self):
    """Test if test user login to admin view"""
    login = self.client.login(username=self.user.username, password='motdepasse')
    self.client.logout()
    self.assertTrue(login)

class test_admin(TestCase):
  """Test """
  def setUp(self):
    """Create test client"""
    self.client = Client()
    """Create object test user"""
    self.user = User.objects.create(username='testuser')
    self.user.set_password('motdepasse')
    self.user.save()
  def test_admin(self):
    login = self.client.login(username=self.user.username, password='motdepasse')
    if not login:
      self.assertTrue(login)
    response = self.client.get('/admin/wallpaper/', follow=True)
    #print(dir(response.context))
    self.assertEqual(response.status_code, 200)
