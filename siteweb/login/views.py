from django.shortcuts import render

# Create your views here.
def login(request):
  """
  Render login windows
  """
  return render(request, 'login/login.html')
